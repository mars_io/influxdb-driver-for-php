#include "php.h"
#include "php_ini.h"
#include <ext/standard/php_smart_str.h>

#ifndef INFLUXDB_COMMON_H
#define INFLUXDB_COMMON_H

#ifndef NULL
#define NULL ((void *) 0)
#endif

#define influxdb_sock_name "Influxdb Socket Buffer"
#define INFLUXDB_SOCK_STATUS_FAILED       0
#define INFLUXDB_SOCK_STATUS_DISCONNECTED 1
#define INFLUXDB_SOCK_STATUS_UNKNOWN      2
#define INFLUXDB_SOCK_STATUS_CONNECTED    3

/* options */
#define INFLUXDB_OPT_SERIALIZER         1
#define INFLUXDB_OPT_PREFIX             2
#define INFLUXDB_OPT_READ_TIMEOUT       3
#define INFLUXDB_OPT_SCAN               4

/* cluster options */
#define INFLUXDB_OPT_FAILOVER           5
#define INFLUXDB_FAILOVER_NONE          0
#define INFLUXDB_FAILOVER_ERROR         1
#define INFLUXDB_FAILOVER_DISTRIBUTE    2

/* serializers */
#define INFLUXDB_SERIALIZER_NONE        0
#define INFLUXDB_SERIALIZER_PHP         1
#define INFLUXDB_SERIALIZER_IGBINARY    2

/* SCAN options */
#define INFLUXDB_SCAN_NORETRY 0
#define INFLUXDB_SCAN_RETRY 1

#ifdef PHP_WIN32
#define PHP_INFLUXDB_API __declspec(dllexport)
#else
#define PHP_INFLUXDB_API
#endif

#define INFLUXDB_URL_MAX_SIZE 1024

#define IS_LEX_ARG(s,l) \
    (l>0 && (*s=='(' || *s=='[' || (l==1 && (*s=='+' || *s=='-'))))

#define HTTP_HEADER(i, p, m, cl, db, sql) \
    strcpy(path, "GET /query?q="); \
    strcat(path, sql); \
    strcat(path, db); \
    strcat(path, " HTTP/1.1\n"); \
    strcat(path, "Content-Type:application/x-www-form-urlencoded\n"); \
    strcat(path, "Host: "); \
    strcat(path, i); \
    strcat(path, ":"); \
    strcat(path, p); \
    strcat(path, "\n\n");

typedef enum {ATOMIC, MULTI, PIPELINE} influxdb_mode;

typedef struct fold_item {
    zval * (*fun)(INTERNAL_FUNCTION_PARAMETERS, void *, ...);
    void *ctx;
    struct fold_item *next;
} fold_item;

typedef struct request_item {
    char *request_str;
    int request_size; /* size_t */
    struct request_item *next;
} request_item;



typedef struct {
    php_stream     *stream;
    char           *host;
    short          port;
    char           *auth;
    double         timeout;
    double         read_timeout;
    long           retry_interval;
    int            failed;
    int            status;
    int            persistent;
    int            watching;
    char           *persistent_id;

    int            serializer;
    long           dbNumber;
    char           *dbName;

    char           *prefix;
    int            prefix_len;

    influxdb_mode     mode;
    fold_item      *head;
    fold_item      *current;

    request_item   *pipeline_head;
    request_item   *pipeline_current;

    char           *err;
    int            err_len;
    zend_bool      lazy_connect;

    int            scan;

    int            readonly;

} InfluxdbSock;

zend_class_entry *influxdb_ce;
zend_class_entry *influxdb_exception_ce;
zend_class_entry *spl_ce_RuntimeException;
#endif
