# Influxdb PHP Extension #

The php influxdb extension provides an API for communicating with the [Influxdb](http://www.influxdata.com/) Time-Series Data
 store. It is released under the [PHP License, version 3.01](http://www.php.net/license/3_01.txt).

#### *Notice that, this is In debugging, it can not be used in production environment. 
#### The code wrote in 2016 and there was no much time for maintenance during this period. And will be consider refactoring in the future.

# Installing/Configuring #
-----

Everything you should need to install influxedb on your system.

## Installation ##

~~~
phpize
./configure
make && make install
~~~

`make install` copies `influxdb.so` to an appropriate location, but you still need to enable the module in the PHP config file. To do so, either edit your php.ini in `/etc/php.d/conf.d` with the following contents: `extension=influxdb.so`.

This extension exports a single class, [Influxdb](#class-influxdb) (and [InfluxdbException](#class-influxdbexception) used in case of errors). 


## Installation on OSX ##

If the install fails on OSX, type the following commands in your shell before trying again:
~~~
MACOSX_DEPLOYMENT_TARGET=10.6
CFLAGS="-arch i386 -arch x86_64 -g -Os -pipe -no-cpp-precomp"
CCFLAGS="-arch i386 -arch x86_64 -g -Os -pipe"
CXXFLAGS="-arch i386 -arch x86_64 -g -Os -pipe"
LDFLAGS="-arch i386 -arch x86_64 -bind_at_load"
export CFLAGS CXXFLAGS LDFLAGS CCFLAGS MACOSX_DEPLOYMENT_TARGET
~~~
