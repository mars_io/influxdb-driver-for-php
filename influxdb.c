/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2015 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "common.h"
#include "library.h"

#include "php.h"
#include "php_influxdb.h"

#include "ext/standard/url.h"

/* If you declare any globals in php_influxdb.h uncomment this:
ZEND_DECLARE_MODULE_GLOBALS(influxdb)
*/

/* {{{ influxdb_functions[]
 *
 * Every user visible function must have an entry in influxdb_functions[].
 */
const zend_function_entry influxdb_functions[] = {

     PHP_ME(Influxdb, __construct, NULL, ZEND_ACC_CTOR | ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, __destruct, NULL, ZEND_ACC_DTOR | ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, connect, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, pconnect, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, close, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, selectdb, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, ping, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, query, NULL, ZEND_ACC_PUBLIC)
     PHP_ME(Influxdb, write, NULL, ZEND_ACC_PUBLIC)

     /* Alias */
     PHP_MALIAS(Influxdb, open, connect, NULL, ZEND_ACC_PUBLIC)

	PHP_FE_END	/* Must be the last line in influxdb_functions[] */
};
/* }}} */

/* {{{ influxdb_module_entry
 */
zend_module_entry influxdb_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
	STANDARD_MODULE_HEADER,
#endif
	"influxdb",
	influxdb_functions,
	PHP_MINIT(influxdb),
	PHP_MSHUTDOWN(influxdb),
	PHP_RINIT(influxdb),		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(influxdb),	/* Replace with NULL if there's nothing to do at request end */
	PHP_MINFO(influxdb),
#if ZEND_MODULE_API_NO >= 20010901
	PHP_INFLUXDB_VERSION,
#endif
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_INFLUXDB
ZEND_GET_MODULE(influxdb)
#endif

/* {{{ PHP_INI
 */
/* Remove comments and fill if you need to have entries in php.ini
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("influxdb.global_value",      "42", PHP_INI_ALL, OnUpdateLong, global_value, zend_influxdb_globals, influxdb_globals)
    STD_PHP_INI_ENTRY("influxdb.global_string", "foobar", PHP_INI_ALL, OnUpdateString, global_string, zend_influxdb_globals, influxdb_globals)
PHP_INI_END()
*/
/* }}} */

/* {{{ php_influxdb_init_globals
 */
/* Uncomment this function if you have INI entries
static void php_influxdb_init_globals(zend_influxdb_globals *influxdb_globals)
{
	influxdb_globals->global_value = 0;
	influxdb_globals->global_string = NULL;
}
*/
/* }}} */

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(influxdb)
{
    struct timeval tv;
	/* If you have INI entries, uncomment these lines
	REGISTER_INI_ENTRIES();
	*/
    zend_class_entry influxdb_class_entry;
    zend_class_entry influxdb_exception_class_entry;

    gettimeofday(&tv, NULL);
    srand(tv.tv_usec *tv.tv_sec);

    INIT_CLASS_ENTRY(influxdb_class_entry, "Influxdb", influxdb_functions);
    influxdb_ce = zend_register_internal_class(&influxdb_class_entry TSRMLS_CC);

    INIT_CLASS_ENTRY(influxdb_exception_class_entry, "InfluxdbException", NULL);
    influxdb_exception_ce = zend_register_internal_class_ex(
        &influxdb_exception_class_entry,
        influxdb_get_exception_base(0 TSRMLS_CC),
        NULL TSRMLS_CC
    );

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(influxdb)
{
	/* uncomment this line if you have INI entries
	UNREGISTER_INI_ENTRIES();
	*/
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request start */
/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(influxdb)
{
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request end */
/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(influxdb)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(influxdb)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "Influxdb Support", "enabled");
	php_info_print_table_row(2, "Influxdb Version", PHP_INFLUXDB_VERSION);
	php_info_print_table_end();

	/* Remove comments if you have entries in php.ini*/
	//DISPLAY_INI_ENTRIES();
    /*
	*/
}
/* }}} */

/* The previous line is meant for vim and emacs, so it can correctly fold and
   unfold functions in source code. See the corresponding marks just before
   function definition, where the functions purpose is also documented. Please
   follow this convention for the convenience of others editing your code.
*/

PHP_METHOD(Influxdb, __construct)
{
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
       RETURN_FALSE;
    }

}

PHP_METHOD(Influxdb, __destruct)
{
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "") == FAILURE) {
        RETURN_FALSE;
    }
}

PHP_METHOD(Influxdb, connect)
{
    if (influxdb_connect(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0) == FAILURE) {
        RETURN_FALSE;
    } else {
        return SUCCESS;
    }
}

PHP_METHOD(Influxdb, pconnect)
{
    if (influxdb_connect(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1) == FAILURE) {
        RETURN_FALSE;
    } else {
        RETURN_TRUE;
    }
}


PHP_METHOD(Influxdb, close)
{
    RETURN_TRUE;
}

PHP_METHOD(Influxdb, selectdb)
{
    InfluxdbSock *influxdb_sock;
    zval *object;
    char* dbname;
    int dbnamelen;
    char dbstr[INFLUXDB_URL_MAX_SIZE * 2] = "&db=\0";
     if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os",
                 &object, influxdb_ce, &dbname, &dbnamelen) == FAILURE) {
        RETURN_FALSE;
    }
    if (influxdb_sock_get(object, &influxdb_sock TSRMLS_CC, 0) < 0) {
        RETURN_FALSE;
    }
    strcat(dbstr,dbname);
    strcat(dbstr, "\0");
    influxdb_sock->dbName = dbstr;
}

PHP_METHOD(Influxdb, query)
{
    InfluxdbSock *influxdb_sock;
    zval *object;
    int len = 1024, slen, qstrlen;
    char resp[len], *str, *qstr;
    char *path = NULL;
    path = (char *)malloc(sizeof(char) * 1024);


    if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os",
                &object, influxdb_ce, &qstr, &qstrlen) == FAILURE) {
        RETURN_FALSE;
    }
    if (influxdb_sock_get(object, &influxdb_sock TSRMLS_CC, 0) < 0) {
        RETURN_FALSE;
    }
    slen = spprintf(&str, 0, qstr);
    qstr = php_url_encode(str, slen, 0);
    char *port;
    spprintf(&port, 0, "%d", influxdb_sock->port);
    HTTP_HEADER(influxdb_sock->host, port, "GET", "", influxdb_sock->dbName, qstr);

    php_stream_write_string(influxdb_sock->stream, path);
    size_t bufl = 0;
    char *buf = (char *)malloc(EXEC_INPUT_BUF);
    //while ((bufl = php_stream_read(influxdb_sock->stream, buf, EXEC_INPUT_BUF)) > 0 ) {
        //slen = spprintf(buf1, 0, buf);
        //RETURN_STRINGL(buf1, slen, 0);
     //   PHPWRITE(buf, bufl);
    //}
    free(buf);

    influxdb_string_response(INTERNAL_FUNCTION_PARAM_PASSTHRU, influxdb_sock,
                NULL, NULL);

    //php_stream_close(influxdb_sock->stream);
}

PHP_METHOD(Influxdb, write)
{
     InfluxdbSock *influxdb_sock;
    zval *object;
    int len = 1024, slen, qstrlen;
    char resp[len], *str, *qstr;
    char path[INFLUXDB_URL_MAX_SIZE * 2];

    if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os",
                &object, influxdb_ce, &qstr, &qstrlen) == FAILURE) {
        RETURN_FALSE;
    }
    if (influxdb_sock_get(object, &influxdb_sock TSRMLS_CC, 0) < 0) {
        RETURN_FALSE;
    }

	slen = spprintf(&str, 0, qstr);

     path[0] = '\0';
    strncat(path, "POST /write?db=_internal", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, " HTTP/1.1\n", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "Host: ", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, influxdb_sock->host, INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, ":", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "8086\n", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "Content-Length: 39", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "\n\n", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, qstr, INFLUXDB_URL_MAX_SIZE * 2);

    php_stream_write_string(influxdb_sock->stream, path);
    //php_stream_read(influxdb_sock->stream, resp, len);
    RETURN_TRUE;

}

PHP_METHOD(Influxdb, ping) {
    InfluxdbSock *influxdb_sock;
    zval *object;
    int len = 1024, slen, qstrlen;
    char resp[len], *str, *qstr;
    char path[INFLUXDB_URL_MAX_SIZE * 2];

    if (zend_parse_method_parameters(ZEND_NUM_ARGS() TSRMLS_CC, getThis(), "Os",
                &object, influxdb_ce, &qstr, &qstrlen) == FAILURE) {
        RETURN_FALSE;
    }
    if (influxdb_sock_get(object, &influxdb_sock TSRMLS_CC, 0) < 0) {
        RETURN_FALSE;
    }

    path[0] = '\0';
    strncat(path, "GET /ping", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, " HTTP/1.1\n", INFLUXDB_URL_MAX_SIZE * 2);
    //strncat(path, "Content-Type:application/x-www-form-urlencoded\n", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "Host: ", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, influxdb_sock->host, INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, ":", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "8086", INFLUXDB_URL_MAX_SIZE * 2);
    strncat(path, "\n\n", INFLUXDB_URL_MAX_SIZE * 2);


    php_stream_write_string(influxdb_sock->stream, path);
    php_stream_read(influxdb_sock->stream, resp, len);
	slen = spprintf(&str, 0, resp);
    RETURN_STRINGL(str, slen, 0);
}

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
