#ifndef INFLUXDB_LIBRARY_H
#define INFLUXDB_LIBRARY_H

PHP_INFLUXDB_API zend_class_entry *influxdb_get_exception_base(int root TSRMLS_DC);

PHP_INFLUXDB_API void influxdb_free_socket(InfluxdbSock *influxdb_sock);

PHP_INFLUXDB_API InfluxdbSock *influxdb_sock_create(char *host, int host_len,
                    unsigned short port, double timeout, int persistent,
                    char *persistent_id, long retry_interval, zend_bool lazy_connect);

PHP_INFLUXDB_API int influxdb_sock_get(zval *id, InfluxdbSock **influxdb_sock TSRMLS_DC,
                          int no_throw);

PHP_INFLUXDB_API char *influxdb_sock_read_bulk_reply(InfluxdbSock *influxdb_sock, int bytes TSRMLS_DC);

PHP_INFLUXDB_API int influxdb_sock_connect(InfluxdbSock *influxdb_sock TSRMLS_DC);

PHP_INFLUXDB_API int influxdb_sock_server_open(InfluxdbSock *influxdb_sock, int force_connect TSRMLS_DC);

PHP_INFLUXDB_API int influxdb_sock_disconnect(InfluxdbSock *influxdb_sock TSRMLS_DC);

PHP_INFLUXDB_API int influxdb_connect(INTERNAL_FUNCTION_PARAMETERS, int persistent);

#endif
