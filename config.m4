dnl $Id$
dnl config.m4 for extension influxdb

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(influxdb, for influxdb support,
dnl Make sure that the comment is aligned:
dnl [  --with-influxdb             Include influxdb support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(influxdb, whether to enable influxdb support,
dnl Make sure that the comment is aligned:
[  --enable-influxdb           Enable influxdb support])

if test "$PHP_INFLUXDB" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-influxdb -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/influxdb.h"  # you most likely want to change this
  dnl if test -r $PHP_INFLUXDB/$SEARCH_FOR; then # path given as parameter
  dnl   INFLUXDB_DIR=$PHP_INFLUXDB
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for influxdb files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       INFLUXDB_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$INFLUXDB_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the influxdb distribution])
  dnl fi

  dnl # --with-influxdb -> add include path
  dnl PHP_ADD_INCLUDE($INFLUXDB_DIR/include)

  dnl # --with-influxdb -> check for lib and symbol presence
  dnl LIBNAME=influxdb # you may want to change this
  dnl LIBSYMBOL=influxdb # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $INFLUXDB_DIR/$PHP_LIBDIR, INFLUXDB_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_INFLUXDBLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong influxdb lib version or lib not found])
  dnl ],[
  dnl   -L$INFLUXDB_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(INFLUXDB_SHARED_LIBADD)

  PHP_NEW_EXTENSION(influxdb, influxdb.c library.c, $ext_shared)
fi
if test -z "$PHP_DEBUG"; then   
    AC_ARG_ENABLE(debug,  
    [   --enable-debug          compile with debugging symbols ],[  
        PHP_DEBUG=$enableval  
    ],[ PHP_DEBUG=no  
    ])  
fi  
