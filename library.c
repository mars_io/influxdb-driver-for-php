#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "common.h"
#include "php_network.h";
#include <sys/types.h>
#ifndef _MSC_VER
#include <netinet/tcp.h>
#include <sys/socket.h>
#endif
#include <ext/standard/php_smart_str.h>
#include <ext/standard/php_var.h>
#include <zend_exceptions.h>
#include "php_influxdb.h"
#include "library.h"
#include <ext/standard/php_math.h>
#include <ext/standard/php_rand.h>

static int le_influxdb;
int le_influxdb_sock;
extern zend_class_entry *spl_ce_RuntimeException = NULL;

PHP_INFLUXDB_API zend_class_entry *influxdb_get_exception_base(int root TSRMLS_DC)
{
#if HAVE_SPL
        if (!root) {
                if (!spl_ce_RuntimeException) {
                        zend_class_entry **pce;

                        if (zend_hash_find(CG(class_table), "runtimeexception",
                                           sizeof("RuntimeException"),
                                           (void**)&pce) == SUCCESS)
                        {
                                spl_ce_RuntimeException = *pce;
                                return *pce;
                        }
                } else {
                        return spl_ce_RuntimeException;
                }
        }
#endif
#if (PHP_MAJOR_VERSION == 5) && (PHP_MINOR_VERSION < 2)
        return zend_exception_get_default();
#else
        return zend_exception_get_default(TSRMLS_C);
#endif
}


PHP_INFLUXDB_API void influxdb_free_socket(InfluxdbSock *influxdb_sock)
{
    if(influxdb_sock->prefix) {
        efree(influxdb_sock->prefix);
    }
    if(influxdb_sock->err) {
        efree(influxdb_sock->err);
    }
    if(influxdb_sock->auth) {
        efree(influxdb_sock->auth);
    }
    if(influxdb_sock->persistent_id) {
        efree(influxdb_sock->persistent_id);
    }
    efree(influxdb_sock->host);
    efree(influxdb_sock);
}

PHP_INFLUXDB_API InfluxdbSock *influxdb_sock_create(char *host, int host_len, unsigned short port, double timeout,
                  int persistent, char *persistent_id, long retry_interval,
                  zend_bool lazy_connect)
{
    InfluxdbSock *influxdb_sock;

    influxdb_sock         = ecalloc(1, sizeof(InfluxdbSock));
    influxdb_sock->host   = estrndup(host, host_len);
    influxdb_sock->stream = NULL;
    influxdb_sock->status = INFLUXDB_SOCK_STATUS_DISCONNECTED;
    influxdb_sock->watching = 0;
    influxdb_sock->dbNumber = 0;
    influxdb_sock->retry_interval = retry_interval * 1000;
    influxdb_sock->persistent = persistent;
    influxdb_sock->lazy_connect = lazy_connect;

    if(persistent_id) {
        size_t persistent_id_len = strlen(persistent_id);
        influxdb_sock->persistent_id = ecalloc(persistent_id_len + 1, 1);
        memcpy(influxdb_sock->persistent_id, persistent_id, persistent_id_len);
    } else {
        influxdb_sock->persistent_id = NULL;
    }

    memcpy(influxdb_sock->host, host, host_len);
    influxdb_sock->host[host_len] = '\0';

    influxdb_sock->port    = port;
    influxdb_sock->timeout = timeout;
    influxdb_sock->read_timeout = timeout;

    influxdb_sock->serializer = INFLUXDB_SERIALIZER_NONE;
    influxdb_sock->mode = ATOMIC;
    influxdb_sock->head = NULL;
    influxdb_sock->current = NULL;
    influxdb_sock->pipeline_head = NULL;
    influxdb_sock->pipeline_current = NULL;

    influxdb_sock->err = NULL;
    influxdb_sock->err_len = 0;

    influxdb_sock->scan = INFLUXDB_SCAN_NORETRY;

    influxdb_sock->readonly = 0;

    return influxdb_sock;
}


PHP_INFLUXDB_API int influxdb_sock_get(zval *id, InfluxdbSock **influxdb_sock TSRMLS_DC,
                          int no_throw)
{

    zval **socket;
    int resource_type;

    if (Z_TYPE_P(id) != IS_OBJECT || zend_hash_find(Z_OBJPROP_P(id), "socket",
        sizeof("socket"), (void **) &socket) == FAILURE)
    {
        // Throw an exception unless we've been requested not to
        if(!no_throw) {
            zend_throw_exception(influxdb_exception_ce, "Influxdb server went away",
                0 TSRMLS_CC);
        }
        return -1;
    }

    *influxdb_sock = (InfluxdbSock *)zend_list_find(Z_LVAL_PP(socket),
        &resource_type);

    if (!*influxdb_sock || resource_type != le_influxdb_sock) {
        // Throw an exception unless we've been requested not to
        if(!no_throw) {
            zend_throw_exception(influxdb_exception_ce, "Influxdb server went away",
                0 TSRMLS_CC);
        }
        return -1;
    }
    if ((*influxdb_sock)->lazy_connect)
    {
        (*influxdb_sock)->lazy_connect = 0;
        if (influxdb_sock_server_open(*influxdb_sock, 1 TSRMLS_CC) < 0) {
            return -1;
        }
    }

    return Z_LVAL_PP(socket);
}

PHP_INFLUXDB_API char *influxdb_sock_read(InfluxdbSock *influxdb_sock, int *buf_len TSRMLS_DC)
{
    char inbuf[1024];
    char *resp = NULL;
    char *totalbuf = NULL;
    size_t err_len;
    memset(inbuf, 0, 1024);
    int i = 0;
    int *returned_len;
    while(1){
        php_stream_get_line(influxdb_sock->stream, inbuf, 1024, NULL);

        if (i == 8){
            break;
        }
       i++;
    }
    char *t;
    int to;
    spprintf(&t, 0, "0x%s", inbuf);
    to = strtol(t, NULL, 16);

    char tobuf[to + 1];
    memset(tobuf, 0, to + 1);
    php_stream_gets(influxdb_sock->stream, tobuf, to + 1);
    php_stream_close(influxdb_sock->stream);
    tobuf[to + 1] = '\0';
    char *er = NULL;
    int tl = spprintf(&er, 0, "%s", tobuf);
    return er;
    //php_stream_read(influxdb_sock->stream, inbuf, 1024);
    //influxdb_parse_info_response(influxdb_sock, inbuf, &totalbuf);
    //resp = influxdb_sock_read_bulk_reply(influxdb_sock, *buf_len TSRMLS_CC);
    //return totalbuf;
}

PHP_INFLUXDB_API void influxdb_parse_info_response(InfluxdbSock* influxdb_sock, char *response, char **totalbuf) {
    /*
    char *cur, *pos, *l, *key;
    cur = response;
    while(1) {
        if(*cur == '\r' && *(cur + 2) == '\r') {
            pos = cur + 4;
            break;
        }else{
            cur++;
            continue;
        }
    }
    l = strchr(pos, '\r');
    int len = (int)(l - pos);
    key = (char *)emalloc(len);
    memcpy(key, pos, len);
    key[len] = '\0';
    char *t;
    int to;
    spprintf(&t, 0, "0x%s", key);
    to = strtol(t, NULL, 16);
    int total = 1024 + to;
    char buf[to];
    php_stream_read(influxdb_sock->stream, buf, to);
    char *fl = l + 2;
    int buflen = strlen(fl) + to;
    *totalbuf  = (char *)emalloc(buflen + 1);
    memset(*totalbuf, 0, buflen + 1);
    strcat(*totalbuf, fl);
    strcat(*totalbuf, buf);
    *(*totalbuf + buflen - 802) = '\0';
    */
}

PHP_INFLUXDB_API void influxdb_string_response(INTERNAL_FUNCTION_PARAMETERS, InfluxdbSock *influxdb_sock, zval *z_tab, void *ctx) {

    char *response;
    int response_len;

    if ((response = influxdb_sock_read(influxdb_sock, &response_len TSRMLS_CC))
                                    == NULL)
    {
        RETURN_FALSE;
    }else{
        RETURN_STRINGL(response, strlen(response), 0);
        efree(response);
    }
}

PHP_INFLUXDB_API char *influxdb_sock_read_bulk_reply(InfluxdbSock *influxdb_sock, int bytes TSRMLS_DC)
{
    int offset = 0;
    size_t got;

    char * reply;


    if (bytes == -1) {
        return NULL;
    } else {
        char c;
        int i;

        reply = emalloc(bytes+1);

        while(offset < bytes) {
            got = php_stream_read(influxdb_sock->stream, reply + offset,
                bytes-offset);
            if (got <= 0) {
                /* Error or EOF */
//                zend_throw_exception
                break;
            }
            offset += got;
        }
        for(i = 0; i < 2; i++) {
            php_stream_read(influxdb_sock->stream, &c, 1);
        }
    }

    reply[bytes] = 0;
    return reply;
}

PHP_INFLUXDB_API int influxdb_sock_connect(InfluxdbSock *influxdb_sock TSRMLS_DC)
{
    struct timeval tv, read_tv, *tv_ptr = NULL;
    char *host = NULL, *persistent_id = NULL;
	//zend_string *errstr;
	char *errstr;
    const char *fmtstr = "%s:%d";
    int host_len, err = 0;
    php_netstream_data_t *sock;
    int tcp_flag = 1;

    if (influxdb_sock->stream != NULL) {
        influxdb_sock_disconnect(influxdb_sock TSRMLS_CC);
    }

    tv.tv_sec  = (time_t)influxdb_sock->timeout;
    tv.tv_usec = (int)((influxdb_sock->timeout - tv.tv_sec) * 1000000);
    if(tv.tv_sec != 0 || tv.tv_usec != 0) {
        tv_ptr = &tv;
    }

    read_tv.tv_sec  = (time_t)influxdb_sock->read_timeout;
    read_tv.tv_usec = (int)((influxdb_sock->read_timeout-read_tv.tv_sec)*1000000);

    if(influxdb_sock->host[0] == '/' && influxdb_sock->port < 1) {
        host_len = spprintf(&host, 0, "unix://%s", influxdb_sock->host);
    } else {
        if(influxdb_sock->port == 0)
            influxdb_sock->port = 6379;

#ifdef HAVE_IPV6
        /* If we've got IPv6 and find a colon in our address, convert to proper
         * IPv6 [host]:port format */
        if (strchr(influxdb_sock->host, ':') != NULL) {
            fmtstr = "[%s]:%d";
        }
#endif
        host_len = spprintf(&host, 0, fmtstr, influxdb_sock->host, influxdb_sock->port);
    }

    if (influxdb_sock->persistent) {
        if (influxdb_sock->persistent_id) {
            spprintf(&persistent_id, 0, "phpinfluxdb:%s:%s", host,
                influxdb_sock->persistent_id);
        } else {
            spprintf(&persistent_id, 0, "phpinfluxdb:%s:%f", host,
                influxdb_sock->timeout);
        }
    }


    influxdb_sock->stream = php_stream_xport_create(host, host_len,
        0, STREAM_XPORT_CLIENT | STREAM_XPORT_CONNECT,
        persistent_id, tv_ptr, NULL, &errstr, &err);


    if (persistent_id) {
        efree(persistent_id);
    }

    efree(host);

    if (!influxdb_sock->stream) {
        if (errstr) efree(errstr);
        return -1;
    }

    /* set TCP_NODELAY */
    sock = (php_netstream_data_t*)influxdb_sock->stream->abstract;

    setsockopt(sock->socket, IPPROTO_TCP, 0, (char *) &tcp_flag,
        sizeof(int));

    php_stream_auto_cleanup(influxdb_sock->stream);

    if(tv.tv_sec != 0 || tv.tv_usec != 0) {
        php_stream_set_option(influxdb_sock->stream,PHP_STREAM_OPTION_READ_TIMEOUT,
            0, &read_tv);
    }
    php_stream_set_option(influxdb_sock->stream,
        PHP_STREAM_OPTION_WRITE_BUFFER, PHP_STREAM_BUFFER_NONE, NULL);

    influxdb_sock->status = INFLUXDB_SOCK_STATUS_CONNECTED;
    //php_stream_write_string(influxdb_sock->stream, "POST /write?db=_internal HTTP/1.1\nHost: 127.0.0.1:8086\nContent-Type: Acceptlication/x-www-form-urlencoded; charset=UTF-8\nContent-Length: 39\n\ncpu_load_short,host=server15 value=0.32");


   return 0;
}

PHP_INFLUXDB_API int influxdb_sock_server_open(InfluxdbSock *influxdb_sock, int force_connect TSRMLS_DC)
{
    int res = -1;
    switch (influxdb_sock->status) {
        case INFLUXDB_SOCK_STATUS_DISCONNECTED:

            return influxdb_sock_connect(influxdb_sock TSRMLS_CC);
        case INFLUXDB_SOCK_STATUS_CONNECTED:

            res = 0;
        break;
        case INFLUXDB_SOCK_STATUS_UNKNOWN:
            if (force_connect > 0 && influxdb_sock_connect(influxdb_sock TSRMLS_CC) < 0) {
                res = -1;
            } else {
                res = 0;

                influxdb_sock->status = INFLUXDB_SOCK_STATUS_CONNECTED;
            }
        break;
    }

    return res;
}

PHP_INFLUXDB_API int influxdb_sock_disconnect(InfluxdbSock *influxdb_sock TSRMLS_DC)
{
    if (influxdb_sock == NULL) {
        return 1;
    }

    influxdb_sock->dbNumber = 0;
    if (influxdb_sock->stream != NULL) {
        influxdb_sock->status = INFLUXDB_SOCK_STATUS_DISCONNECTED;
            influxdb_sock->watching = 0;

            /* Stil valid? */
            if(influxdb_sock->stream && !influxdb_sock->persistent) {
                php_stream_close(influxdb_sock->stream);
            }
            influxdb_sock->stream = NULL;

            return 1;
    }

    return 0;
}




PHP_INFLUXDB_API int influxdb_connect(INTERNAL_FUNCTION_PARAMETERS, int persistent) {

    zval *object;
    zval **socket;
    int host_len, id;
    char *host = NULL;
    long port = -1;
    long retry_interval = 0;

    char *persistent_id = NULL;
    int persistent_id_len = -1;

    double timeout = 0.0;
    InfluxdbSock *influxdb_sock  = NULL;

#ifdef ZTS
    persistent = 0;
#endif

    if (zend_parse_method_parameters(ZEND_NUM_ARGS(), getThis(),
                                     "Os|ldsl", &object, influxdb_ce, &host,
                                     &host_len, &port, &timeout, &persistent_id,
                                     &persistent_id_len, &retry_interval)
                                     == FAILURE)
    {
        return FAILURE;
    }

    /* If it's not a unix socket, set to default */
    if(port == -1 && host_len && host[0] != '/') {
        port = 8086;
    }

    if (influxdb_sock_get(object, &influxdb_sock TSRMLS_CC, 1) > 0) {
        if ((socket = zend_hash_find(Z_OBJPROP_P(object), "socket", sizeof("socket"), (void **) &socket)) == FAILURE)
        {
        } else {
            zend_list_delete(Z_RES_P(socket));
        }
    }

    influxdb_sock = influxdb_sock_create(host, host_len, port, timeout, persistent,
        persistent_id, retry_interval, 0);


    if (influxdb_sock_server_open(influxdb_sock, 1 TSRMLS_CC) < 0) {
        influxdb_free_socket(influxdb_sock);
        //zend_throw_exception(influxdb_exception_ce, "connect valid", 0 TSRMLS_CC);
        return FAILURE;
    }


    #if PHP_VERSION_ID >= 50400
        id = zend_list_insert(influxdb_sock, le_influxdb_sock TSRMLS_CC);
    #else
        id = zend_list_insert(influxdb_sock, le_influxdb_sock);
    #endif

    add_property_resource(object, "socket", id);

    return SUCCESS;
}


